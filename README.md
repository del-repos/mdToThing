# mdToThing
Converts markdown files into either pptx using Marp

## Purpose 
In order to efficiently mass produce pptx from markdown. This was made for the purposes of being able to add

Any organization can use the current templates to fill out their slides and automatically make it into pptx


## Technical Details
This program uses **[marp-cli](https://github.com/marp-team/marp-cli)** as the main _(only)_ way to convert <*.md> files to <*.pptx> files

## Usage
### TO INSTALL
`./install.sh`

### TO RUN
`./mdToThing.sh <*.md file>` 
> Current Method to convert md files
> Just use npx marp <*.md file>, we are still trying with 

## Limitations
Currently the limitations to the program at only what marp is capable of creating.
* Markdown Tables do **not** work
* Be specific with imgs, sizes will not crop to their size.
* Use `---` to denote next slide (page separator)
* `*` Will have a transition
* To not overflow the origin try to keep the page under 879 total characters at the default marp font
* It requires npm and Chrome or Microsoft Edge


## License 
This tool releases under the [MIT License](LICENSE)