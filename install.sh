#!/bin/bash

# SRC CODE:
# https://github.com/marp-team/marp-cli
# Check link for custom setup

echo "======================"
echo "  Local Installation  "
echo "======================"

# Checks if npm is installed
# If installed, download program from npm
if [[ $(command -v npm) ]]; then
    npm install --save-dev @marp-team/marp-cli
    exit
fi

echo "> NPM NOT INSTALLED"
echo "> PLEASE INSTALL NPM"
echo "======================"
