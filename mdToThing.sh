#!/bin/bash

usage () {
    echo "Usage:"
    echo "./mdToThing.sh <*.md file> --theme=<*.css files"
    exit
}


## To Convert md files to pptx
npx marp $1 --pptx --theme css/sig•mal.css 

## Open The newly created file
open test/04.pptx
